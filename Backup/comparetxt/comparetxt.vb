Imports System.IO
Imports System.Text
Imports System.Environment
Imports System.Xml
Imports System.Xml.XPath

Public Class comparetxt
    Dim wssListName As String = "字頻表"
    Dim wssViewName As String = "{02AD0EFF-D224-474E-BBA2-5B7873537488}"
    Dim wssUser As String = "ttcon"
    Dim wssPwd As String = "ttcon"
    Dim wssDomain As String = "hanlin"
    Dim wssXmlName As String = "\wordFrequency.xml"

    Dim stb_name As String = "字頻表" '來源字頻表名稱
    Dim dtb_name As String = "排序字頻" '排序完字頻表名稱

    Dim col0_name As String = "字"
    Dim col1_name As String = "筆數"
    Dim col2_name As String = "序號"
    Dim col3_name As String = "字頻"

    Dim sortStr As String = "筆數 ASC,字頻 DESC" '排序字串


    '課文全新字標題:
    Dim Title_bookNew As String = "課文新字"
    '設定習寫字:
    Dim Title_wWord As String = "設定習寫字"
    '設定認讀字標題:
    Dim Title_wRead As String = "設定認讀字"
    '消化認讀字標題:
    Dim Title_eraseRead As String = "消化認讀字"

#Region "課文檔案清單"
    '加入課文檔案清單項目
    Private Sub bn1More_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles bn1More.Click
        getFilesList(Me.lb1)
    End Sub
    '移除課文檔案清單中指定選擇項目
    Private Sub bn1Less_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles bn1Less.Click
        delListItems(Me.lb1)
    End Sub
    '選定清單項目上移
    Private Sub bn1Up_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles bn1Up.Click
        upRow(Me.lb1)
    End Sub
    '選定清單項目下移
    Private Sub bn1Down_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles bn1Down.Click
        downRow(Me.lb1)
    End Sub
    '清除清單
    Private Sub bn1Clear_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles bn1Clear.Click
        Me.lb1.Items.Clear()
    End Sub
#End Region

#Region "習寫字檔清單"
    '加入課文檔案清單項目
    Private Sub bn2More_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles bn2More.Click
        getFilesList(Me.lb2)
    End Sub
    '移除課文檔案清單中指定選擇項目
    Private Sub bn2Less_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles bn2Less.Click
        delListItems(Me.lb2)
    End Sub
    '選定清單項目上移
    Private Sub bn2Up_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles bn2Up.Click
        upRow(Me.lb2)
    End Sub
    '選定清單項目下移
    Private Sub bn2Down_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles bn2Down.Click
        downRow(Me.lb2)
    End Sub
    '清除清單
    Private Sub bn2Clear_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles bn2Clear.Click
        Me.lb2.Items.Clear()
    End Sub
#End Region

#Region "認讀字檔清單"
    '加入課文檔案清單項目
    Private Sub bn3More_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles bn3More.Click
        getFilesList(Me.lb3)
    End Sub
    '移除課文檔案清單中指定選擇項目
    Private Sub bn3Less_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles bn3Less.Click
        delListItems(Me.lb3)
    End Sub
    '選定清單項目上移
    Private Sub bn3Up_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles bn3Up.Click
        upRow(Me.lb3)
    End Sub
    '選定清單項目下移
    Private Sub bn3Down_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles bn3Down.Click
        downRow(Me.lb3)
    End Sub
#End Region

#Region "listBox 各項操作函數"
    '取得檔案清單 置入指定之ListBox
    Function getFilesList(ByVal inListBox As ListBox) As String
        getFilesList = Nothing
        'inListBox.Items.Clear()
        Dim ret As String()
        Me.OpenInputFile.Filter = "Text files (*.txt)|*.txt"
        If (Me.OpenInputFile.ShowDialog = Windows.Forms.DialogResult.OK) Then
            ret = Me.OpenInputFile.FileNames
            If ret.Length > 0 Then
                Dim v1 As Integer = Nothing
                For v1 = 0 To ret.Length - 1
                    Try
                        inListBox.Items.Add(ret(v1).Substring(ret(v1).LastIndexOf("\") + 1))
                    Catch ex As Exception
                        Me.msg.Text += ex.Message + vbCrLf
                        Return ex.Message
                    End Try
                Next
            End If
        End If
    End Function

    '移除清單中指定選擇項目
    Function delListItems(ByVal inListBox As ListBox) As String
        delListItems = Nothing
        With inListBox
            Dim nowIndex As Integer = .SelectedIndex
            .BeginUpdate()
            .Items.Remove(inListBox.SelectedItem)
            .EndUpdate()
            '刪完之後選則落到哪
            If nowIndex <= .Items.Count - 1 Then
                .SelectedIndex = nowIndex
            Else '最後一筆直接選最後一筆
                .SelectedIndex = .Items.Count - 1
            End If
        End With
    End Function

    '上移清單中指定選擇項目
    Function upRow(ByVal inListBox As ListBox) As String
        upRow = Nothing
        If (Not inListBox.SelectedItem Is Nothing) And (inListBox.SelectedIndex > 0) Then
            With inListBox
                .BeginUpdate()
                Dim nowIdx As Integer = .SelectedIndex
                Dim preIdx As Integer = .SelectedIndex - 1
                'Me.msg.Text += nowIdx.ToString + "  " + preIdx.ToString
                .Items.Insert(preIdx, .SelectedItem)
                .Items.RemoveAt(nowIdx + 1)
                .SelectedItem = .Items.Item(preIdx)
                .EndUpdate()
            End With
        End If
    End Function

    '下移清單中指定選擇項目
    Function downRow(ByVal inListBox As ListBox) As String
        downRow = Nothing
        If (Not inListBox.SelectedItem Is Nothing) And (inListBox.SelectedIndex < inListBox.Items.Count - 1) Then
            With inListBox
                .BeginUpdate()
                Dim nowIdx As Integer = .SelectedIndex
                Dim NextIdx As Integer = .SelectedIndex + 2
                'Me.msg.Text += nowIdx.ToString + "  " + NextIdx.ToString + vbCrLf
                .Items.Insert(NextIdx, .SelectedItem)
                .Items.RemoveAt(nowIdx)
                .SelectedItem = .Items.Item(nowIdx + 1)
                .EndUpdate()
            End With
        End If
    End Function
#End Region

#Region "視窗功能"

    '清除訊息視窗
    Private Sub bnClearMsg_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles bnClearMsg.Click
        Me.msg.Text = Nothing
    End Sub

    '執行
    Private Sub bnRun_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles bnRun.Click
        wssConn() '取得字頻表

        Dim endSymbol As String = "*" '分段符號
        Dim outTxtFileName As String = "outTxt.txt" '外部參考檔名
        Dim outTxtFilePath As String = Nothing '外部參考檔全路徑
        Dim exceptStr() As String = {"", "﹂", "『", "』", "（", "）", "﹖", "…", "；", "︰", "：", "！", " ", "　", ",", "。", "，", "？", "」", "「", "、", "*" _
, Chr(10) + Chr(13), Chr(10), Chr(13), "ㄅ", "ㄆ", "ㄇ", "ㄈ", "ㄉ", "ㄊ", "ㄋ", "ㄌ", "ㄍ", "ㄎ", "ㄏ", "ㄐ", "ㄑ", "ㄒ", "ㄓ", "ㄔ", "ㄕ", "ㄖ", "ㄗ", "ㄘ", "ㄙ", "ㄚ", "ㄛ" _
, "ㄜ", "ㄝ", "ㄞ", "ㄟ", "ㄠ", "ㄡ", "ㄢ", "ㄣ", "ㄤ", "ㄥ", "ㄦ", "ㄧ", "ㄨ", "ㄩ"}

        Try
            If Trim(Me.txtOutput.Text) = Nothing Then '確認外部參考檔路徑是否已經提供
                MessageBox.Show("請輸入輸出檔案路徑")
                Return
            ElseIf File.Exists(Me.txtOutput.Text + "\" + outTxtFileName) = False Then  '檢查外部參考檔是否存在
                outTxtFilePath = Me.txtOutput.Text + "\" + outTxtFileName
                Dim outTxtFile As FileInfo = New FileInfo(outTxtFilePath)
                Dim fs As FileStream = outTxtFile.Create()
                fs.Close()
            Else
                outTxtFilePath = Me.txtOutput.Text + "\" + outTxtFileName
                My.Computer.FileSystem.WriteAllText(outTxtFilePath, "", False, System.Text.Encoding.Default) '清空參考檔
            End If
        Catch ex As Exception
            Me.msg.Text += ex.Message
            MessageBox.Show("建立檔案失敗:" + ex.Message)
            Return
        End Try


        Dim Str2 As String = Nothing '課文全新字
        Dim Str1 As String = Nothing '未習寫過新字
        Dim v1 As Integer

        Me.procBar.Maximum = Me.lb1.Items.Count + 1
        Me.procBar.Value = 0
        Me.procBar.Value += 1
        For v1 = 0 To Me.lb1.Items.Count - 1 '依清單順序取得個課文檔路徑
            Dim bookFilePath As String = Me.lb1.Items.Item(v1).ToString '取得完整路徑
            If v1 = 0 Then '清單第一個
                My.Computer.FileSystem.WriteAllText(outTxtFilePath, "", False) '清空參考檔內容
            End If

            '拆解課
            Dim allBooks() As String = pharseLesson(bookFilePath, endSymbol)
            Dim lessonText As String = Nothing
            Dim v2 As Integer
            For v2 = 0 To allBooks.Length - 1 '逐課作業
                '取得每課內容
                lessonText = allBooks(v2)
                lessonText = repeatFilter(lessonText) '過濾重覆字
                lessonText = symbolFilter(lessonText, exceptStr) '過濾例外字
                'Me.msg.Text += "課文字: " + lessonText + vbCrLf

                '讀入參考文字檔內 累積內容   outStr()
                Dim outStr() As String = readPreBook(outTxtFilePath, ":")

                '由習字檔清單取得對應之習字檔並傳回內容wStr
                Dim wStr As String = readwWord(Me.lb2, endSymbol, v1, v2)
                wStr = symbolFilter(wStr, exceptStr) '過濾例外字

                '由認讀字檔清單取得對應之認讀字檔並傳回內容rStr
                Dim rStr As String = readrWord(Me.lb3, endSymbol, v1, v2)
                rStr = symbolFilter(rStr, exceptStr) '過濾例外字

                '比對 該課文 vs 歷屆習寫字+本課習寫字 找出未學字Str1
                Str1 = comp_lesson2w(lessonText, outStr(1) + wStr)
                '分辨習寫字 及 認讀字
                Dim sugRlt() As String = filterWR(Str1, v1 + 1)
                Dim sug_Write As String = ""
                Dim sug_Read As String = ""
                If Not sugRlt Is Nothing Then
                    If sugRlt.Length = 2 Then
                        sug_Write = sugRlt(0) '排序過的未習寫過字(可寫入的)
                        sug_Read = sugRlt(1) '排序過的認讀字(可寫入的)
                    End If
                End If

                '從Str1中以sug_Write為參考挑出依課文順序排的習寫字
                Dim noneSort_Write As String = pickupStr(Str1, sug_Write)

                'erase_Read 判別設定習字中，哪些是消化以前認讀字
                Dim erase_Read As String = pickupStr(wStr, outStr(2) + rStr)
                'erase_Read 再過濾 去除出現過的消化認讀字
                erase_Read = revpickupStr(erase_Read, outStr(3))
                '消化認讀字再過濾，當冊當課文有出現的字才出現
                'erase_Read = pickupStr(erase_Read, lessonText)

                '比對課文新字
                Str2 = comp_allBook(lessonText, outStr(0))

                '比對當冊當課設定習字與課文全新字 找出消失的習寫字
                Dim away_wStr As String = revpickupStr(wStr, Str2)
                '消失習字再過濾，消化認讀字沒有出現的才顯示
                away_wStr = revpickupStr(away_wStr, erase_Read)

                '寫入參考檔
                Try
                    writein(outTxtFilePath, v1 + 1, v2 + 1, Trim(Str1), Trim(Str2), Trim(wStr), Trim(sug_Write), Trim(sug_Read), noneSort_Write, erase_Read, Trim(rStr), Trim(away_wStr))
                Catch ex As Exception
                    MessageBox.Show(ex.Message)
                    Return
                End Try
            Next
            Me.procBar.Value += 1
        Next
        MessageBox.Show("ＯＫ！")
    End Sub

    '選擇輸出檔案夾
    Private Sub bn_openFolder_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles bn_openFolder.Click
        If Me.openFolder.ShowDialog = Windows.Forms.DialogResult.OK Then
            Me.txtOutput.Text = Me.openFolder.SelectedPath
        End If
    End Sub


    Private Sub bn_Array_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles bn_Array.Click
        wssConn()
        'sortbyView("同理如果一年級有十二")
        filterWR("子吧的長雨要春真啦發種芽樹", 2)

    End Sub

    '===================================================================================================================
    'FUNCTION

    '分析該課文檔。傳回課文陣列，一課為一個元素
    Function pharseLesson(ByVal bookFilePath As String, ByVal sepLesson As String) As String()
        pharseLesson = Nothing
        Dim allBookStr As String = My.Computer.FileSystem.ReadAllText(bookFilePath, System.Text.Encoding.Default)
        Dim allBooks() As String = allBookStr.Split(sepLesson)
        Dim v1 As Integer
        For v1 = 0 To allBooks.Length - 1
            allBooks(v1) = Trim(allBooks(v1))
        Next

        pharseLesson = allBooks
    End Function

#End Region

#Region "嘗試 從WSS網站取得字頻表所有資料並存放在我的文件夾內"

    '從sharepoint網站取得字頻表資料並存入檔案
    Function wssConn() As String
        wssConn = Nothing
        Try
            '從sharepoint 取得資料
            Dim getData As New listService.Lists
            Dim xn As XmlNode = Nothing

            getData.Credentials = New System.Net.NetworkCredential(wssUser, wssPwd, wssDomain)  'nc
            xn = getData.GetListItems(wssListName, wssViewName, Nothing, Nothing, 0, Nothing)
            '寫入XML檔
            Dim xmlDoc As New XmlDocument
            xmlDoc.LoadXml(xn.OuterXml)
            Dim saveXML_Path As String = My.Computer.FileSystem.SpecialDirectories.MyDocuments '我的文件夾路徑
            xmlDoc.Save(saveXML_Path + wssXmlName) '存檔
        Catch ex As Exception
            Return ex.Message
        End Try
    End Function

#End Region

#Region "過濾課文內容 過濾課文內容去除重覆字&指定符號"

    '過濾重覆字
    Function repeatFilter(ByVal Str As String) As String
        repeatFilter = Nothing
        If Str Is Nothing Then
            Return Nothing
        End If

        If Str.Length <= 0 Then
            Return Nothing
        End If

        Dim tmpStr As String = Nothing '暫存字串
        tmpStr = Str(0) 'lessonText 第 1 字寫入tmpStr
        If Str.Length >= 2 Then
            Dim v1 As Integer
            For v1 = 1 To Str.Length - 1 '從 lessonText 第2個字開始
                Dim myStr As String = Str(v1) '取得字元內容
                Dim addIf As Boolean = True '是否加入tmpStr布林
                Dim v2 As Integer
                For v2 = 0 To tmpStr.Length - 1 '從 tmpStr 第 1 個字開始
                    If tmpStr(v2) = myStr Then 'tmpStr目前字內容與目前lessonText字內容同否?
                        addIf = False
                        Exit For
                    End If
                Next
                If addIf = True Then 'tmpStr布林是否為true
                    tmpStr += myStr '加入 tmpStr 
                End If
            Next
        End If

        Return tmpStr
    End Function

    '過濾例外字
    Function symbolFilter(ByVal Str As String, ByVal exceptStr() As String) As String
        symbolFilter = Nothing
        If Str Is Nothing Then
            Return Nothing
        End If

        If Str.Length <= 0 Then
            Return Nothing
        End If

        Dim tmpStr As String = Nothing '暫存字串
        If exceptStr Is Nothing Then '如果例外符號陣列為空
            Return Str
        End If

        Dim v1 As Integer
        For v1 = 0 To Str.Length - 1 '從 lessonText 第1個字開始
            Dim addIf As Boolean = True '定義是否加入tmpStr 布林為true
            Dim myStr As String = Str(v1) '取得字元內容
            Dim v2 As Integer
            For v2 = 0 To exceptStr.Length - 1 '從exceptSym()陣列第1個元素開始
                If exceptStr(v2) = myStr Then 'lessonText(i) 與  exceptSym(i)  內容是否相同
                    addIf = False
                    Exit For
                End If
            Next
            If addIf = True Then 'tmpStr 布林是否為true
                tmpStr += myStr
            End If
        Next

        Return tmpStr
    End Function

#End Region

#Region "'讀入參考文字檔內容"

    '讀入參考文字檔內 所有課文全新字 + 設定習字outStr
    Function readPreBook(ByVal outTxtFilePath As String, ByVal sepTitle As String) As String()
        readPreBook = Nothing


        '課文全新字字串暫存變數
        Dim bnewStr As String = Nothing
        '所有習寫字串暫存變數
        Dim wWordStr As String = Nothing
        '所有設定認讀字串暫存變數
        Dim rWordStr As String = Nothing
        '所有消化認讀字串暫存變數
        Dim eraseReadStr As String = Nothing

        Dim allLines() As String = File.ReadAllLines(outTxtFilePath, System.Text.Encoding.Default)
        Dim v1 As Integer
        For v1 = 0 To allLines.Length - 1
            Dim nowLine As String = allLines(v1)
            Dim Rlt() As String = nowLine.Split(sepTitle)
            If Rlt.Length >= 2 Then
                Select Case Trim(Rlt(0))
                    Case Title_bookNew
                        bnewStr += Trim(Rlt(1)) '課文全新字累加
                    Case Title_wWord
                        wWordStr += Trim(Rlt(1)) '習寫字累加
                    Case Title_wRead
                        rWordStr += Trim(Rlt(1)) '設定認讀字累加
                    Case Title_eraseRead
                        eraseReadStr += Trim(Rlt(1)) '消化認讀字累加
                End Select
            End If
        Next

        Dim resultStr() As String = {bnewStr, wWordStr, rWordStr, eraseReadStr}
        readPreBook = resultStr
        'Me.msg.Text = bnewStr + vbCrLf
        'Me.msg.Text += misswWord + vbCrLf
    End Function

#End Region

#Region "由習字檔清單取得對應之習字檔並傳回內容"

    '由習字檔清單取得對應之習字檔並傳回內容wStr
    Function readwWord(ByVal wListBox As ListBox, ByVal sepLesson As String, ByVal bookNum As Integer, ByVal lessonNum As Integer) As String
        readwWord = Nothing
        If bookNum <= (wListBox.Items.Count - 1) Then '習寫字清單項目數量 是否足夠應付 傳來指定項目
            Try
                Dim wWordFilePath As String = wListBox.Items.Item(bookNum)
                Dim wStrs As String = My.Computer.FileSystem.ReadAllText(wWordFilePath, System.Text.Encoding.Default) '讀入所有文字
                Dim wStr() As String = wStrs.Split(sepLesson)
                If wStr.Length >= lessonNum Then
                    readwWord = wStr(lessonNum)
                End If
            Catch ex As Exception
                Me.msg.Text += "讀取第" + bookNum.ToString + "項習字檔，發生錯誤  錯誤訊息:" + ex.Message
                Return Nothing
            End Try
        Else
            Return Nothing
        End If
    End Function

#End Region

#Region "由認讀字檔清單取得對應之認讀字檔並傳回內容"

    '由認讀字檔清單取得對應之認讀字檔並傳回內容wStr
    Function readrWord(ByVal rListBox As ListBox, ByVal sepLesson As String, ByVal bookNum As Integer, ByVal lessonNum As Integer) As String
        readrWord = Nothing
        If bookNum <= (rListBox.Items.Count - 1) Then '認讀字清單項目數量 是否足夠應付 傳來指定項目
            Try
                Dim WordFilePath As String = rListBox.Items.Item(bookNum)
                Dim Strs As String = My.Computer.FileSystem.ReadAllText(WordFilePath, System.Text.Encoding.Default) '讀入所有文字
                Dim Str() As String = Strs.Split(sepLesson)
                If Str.Length >= lessonNum Then
                    readrWord = Str(lessonNum)
                End If
            Catch ex As Exception
                Me.msg.Text += "讀取第" + bookNum.ToString + "項習字檔，發生錯誤  錯誤訊息:" + ex.Message
                Return Nothing
            End Try
        Else
            Return Nothing
        End If
    End Function

#End Region

#Region "'比對 該課文 vs 歷屆習寫字+本課習寫字 找出未學字Str1"

    '比對 該課文 vs 歷屆習寫字+本課習寫字 找出未學字Str1
    Function comp_lesson2w(ByVal nowLesson As String, ByVal allwStr As String) As String
        comp_lesson2w = Nothing

        '沒有課文內容
        If nowLesson Is Nothing Then
            Return Nothing
        End If

        '沒有任何設定習寫字
        If allwStr Is Nothing Then
            Return nowLesson
        End If

        Dim v1 As Integer
        Dim tmpStr As String = Nothing
        '比對所有習寫過的字
        For v1 = 0 To nowLesson.Length - 1
            Dim myStr As String = nowLesson(v1)
            Dim v2 As Integer
            Dim addIf As Boolean = True '定義是否加入 tmpStr 布林變數
            For v2 = 0 To allwStr.Length - 1
                If myStr.ToString = allwStr(v2) Then 'myStr 與 目前 allwStr (i)比較內容是否相同
                    addIf = False
                    Exit For
                End If
            Next
            If addIf = True Then 'Addif 布林變數是否為 True
                tmpStr += myStr 'myStr 加入 tmpStr
            End If
        Next

        Return tmpStr
    End Function

#End Region

#Region "未習寫過字 Str1 分辨習寫字 及 認讀字"
    '未習寫過字 Str1 分辨習寫字 及 認讀字
    Function filterWR(ByVal inputStr As String, ByVal bookNum As Integer) As String()
        filterWR = Nothing
        inputStr = Trim(inputStr)
        If inputStr = Nothing Then
            Return Nothing
        End If
        If inputStr.Length = 0 Then
            Return Nothing
        End If

        '排序( 字 , 筆數 , 序號 , 字頻 )
        Dim tb As DataTable = sortbyView(inputStr)
        Me.dgv.DataSource = tb

        '依據不同年級給予不同認讀字判別條件
        Dim wNum As Integer = 0 '習寫字數限制
        Dim stroke As Integer = 0 '筆數限制
        Dim id As Integer = 0 '序號限制
        Select Case bookNum
            Case 1
                wNum = 10
                stroke = 12
                id = 1000
            Case 2
                wNum = 12
                stroke = 15
                id = 1000
            Case 3
                wNum = 15
                stroke = 18
                id = 1250
            Case 4
                wNum = 18
                stroke = 18
                id = 1250
            Case 5 To 6
                wNum = 20
                stroke = 20
                id = 1649
            Case 7 To 12
                id = 2818
        End Select

        Return limitation(tb, wNum, stroke, id)
    End Function

    '取得字之( 字 , 筆數 , 序號 , 字頻 )
    Function getXmlData(ByVal wordStr As String) As String()
        getXmlData = Nothing
        Dim xmlDoc As New XmlDocument
        xmlDoc.Load(My.Computer.FileSystem.SpecialDirectories.MyDocuments + wssXmlName) '載入XML文件
        Dim xpathStr As String = "//*[@ows_Title='" + wordStr + "']" 'xpath語法
        Dim xn As XmlNode = xmlDoc.SelectSingleNode(xpathStr)

        Dim xa1 As String = Nothing '字
        Dim xa2 As String = Nothing '筆數
        Dim xa3 As String = Nothing '序號
        Dim xa4 As String = Nothing '字頻

        If Not xn Is Nothing Then
            '字
            xa1 = xn.SelectSingleNode("@ows_Title").Value
            '筆數
            xa2 = xn.SelectSingleNode("@ows_penNum").Value
            '序號
            xa3 = xn.SelectSingleNode("@ows_wID").Value
            '字頻
            xa4 = xn.SelectSingleNode("@ows_ariseNum").Value

            Dim collectw() As String = {xa1, xa2, xa3, xa4}
            Return collectw
        Else '沒找到 填入最大數值9999
            '字
            xa1 = wordStr
            '筆數
            xa2 = 9999
            '序號
            xa3 = 9999
            '字頻
            xa4 = 9999

            Dim collectw() As String = {xa1, xa2, xa3, xa4}
            Return collectw
        End If
    End Function

    '透過 DataView排序
    Function sortbyView(ByVal str As String) As DataTable
        sortbyView = Nothing
        Dim ds As New DataSet 'Dataset
        Dim tb As New DataTable 'table
        '欄位
        Dim col0 As New DataColumn '字
        Dim col1 As New DataColumn '筆數
        Dim col2 As New DataColumn '序號
        Dim col3 As New DataColumn '字頻
        '指定欄位名稱 型別
        col0.ColumnName = col0_name
        col1.ColumnName = col1_name
        col1.DataType = System.Type.GetType("System.Int32") '指定數字型別 後排序才會正確
        col2.ColumnName = col2_name
        col2.DataType = System.Type.GetType("System.Int32") '指定數字型別 後排序才會正確
        col3.ColumnName = col3_name
        col3.DataType = System.Type.GetType("System.Int32") '指定數字型別 後排序才會正確
        '建立Table欄位
        tb.Columns.Add(col0)
        tb.Columns.Add(col1)
        tb.Columns.Add(col2)
        tb.Columns.Add(col3)
        '加入資料行
        Dim v5 As Integer
        For v5 = 0 To str.Length - 1
            Dim chr As String = str(v5)
            '由 XML 找到相關資料
            '取得字之筆數、序號、頻率資料
            Dim tmpData() As String = getXmlData(chr)
            If Not tmpData Is Nothing Then
                'table 加入資料
                Dim row1 As DataRow = tb.Rows.Add()
                row1.Item(col0_name) = tmpData(0)
                row1.Item(col1_name) = CType(tmpData(1), Integer) '轉換為數字型態
                row1.Item(col2_name) = CType(tmpData(2), Integer)
                row1.Item(col3_name) = CType(tmpData(3), Integer)
            End If
        Next

        tb.TableName = stb_name '為 table 取名
        ds.Tables.Add(tb) 'table 加入資料集


        '指定給DataView
        dv = ds.Tables(0).DefaultView
        '利用DataView 進行排序
        Me.dv.Sort = sortStr

        Dim newtb As DataTable = dv.ToTable(dtb_name)
        '傳回排序過的新Table表
        Return newtb
    End Function

    '依冊數
    '習寫字數條件 wordNum
    '筆畫數限制 strokeNum
    '序號限制 idNum
    '傳回字串陣列元素(建議習寫子)(建議非習寫字)
    Function limitation(ByVal strTable As DataTable, ByVal wNum As Integer, ByVal stroke As Integer, ByVal id As String) As String()
        limitation = Nothing
        Dim writeStr As String = Nothing '習寫字
        Dim readStr As String = Nothing '認讀字

        Dim v1 As Integer
        For v1 = 0 To strTable.Rows.Count - 1 '行
            Dim row1 As DataRow = strTable.Rows(v1) '欄

            If (stroke = 0) And (id = 0) Then '沒有筆數跟序號限制
                writeStr += row1.Item(col0_name) '沒有限制條件全部都是習寫字

            ElseIf (stroke = 0) And (id <> 0) Then '沒有筆數限制 有序號限制
                If (row1.Item(col2_name) > id) Then '序號大於指定數目打入認讀字
                    readStr += row1.Item(col0_name) '加入認讀字
                Else
                    writeStr += row1.Item(col0_name) '否則加入習寫字
                End If

            ElseIf (stroke <> 0) And (id = 0) Then '有筆數限制 沒序號限制
                If (row1.Item(col1_name) > stroke) Then '如果筆數大於指定數目
                    readStr += row1.Item(col0_name) '打入認讀字
                Else
                    writeStr += row1.Item(col0_name) '加入習寫字
                End If

            ElseIf (stroke <> 0) And (id <> 0) Then '有筆數跟序號限制
                If (row1.Item(1) > stroke) Or (row1.Item(2) > id) Then
                    readStr += row1.Item(col0_name) '加入認讀字
                Else
                    writeStr += row1.Item(col0_name) '加入習寫字
                End If
            End If
        Next

        '檢查習寫字是否超過規定數目
        If wNum <> 0 Then '習寫字有數目限制
            Dim str1 As String = Nothing '新習寫字串
            Dim str2 As String = Nothing '新認讀字串
            If Not writeStr Is Nothing Then '建議習寫字不為空 空亦跳過
                If writeStr.Length > wNum Then
                    str1 = writeStr.Substring(0, wNum) '新習寫字串
                    str2 = writeStr.Substring(wNum) '加入認讀字前面的字串
                    writeStr = str1 '重新定義習寫字串
                    readStr = str2 + readStr '重新定義認讀字串
                End If
            End If
        End If

        'Me.msg.Text += writeStr + vbCrLf
        'Me.msg.Text += readStr + vbCrLf

        Dim str() As String = {writeStr, readStr}
        Return str
    End Function

#End Region

#Region "比對課文檔與參考檔取得課文全新字 Return Str2"

    '比對課文檔與參考檔取得課文全新字 Return Str2
    Function comp_allBook(ByVal nowLesson As String, ByVal allLesson As String) As String
        comp_allBook = Nothing

        'nowLesson 內容是否為空
        If nowLesson Is Nothing Then
            Return Nothing
        ElseIf nowLesson.Length <= 0 Then
            Return Nothing
        End If

        'allLesson 是否為空
        If allLesson Is Nothing Then
            Return nowLesson
        ElseIf allLesson.Length <= 0 Then
            Return nowLesson
        End If

        '定義字串暫存變數 tmpStr
        Dim tmpStr As String = Nothing

        '從nowLesson第一個字開始
        Dim v1 As Integer
        For v1 = 0 To nowLesson.Length - 1
            '取得目前內容 myStr
            Dim myStr As String = nowLesson(v1)
            '是否加入 tmpStr 布林 addif = true
            Dim addif As Boolean = True
            '從allLesson第一個字開始
            Dim v2 As Integer
            For v2 = 0 To allLesson.Length - 1
                'myStr 內容是否等於allLesson(i)
                If myStr.ToString = allLesson(v2) Then
                    addif = False
                    Exit For
                End If
            Next
            '是否 addif = true
            If addif = True Then
                '加入 tmpStr
                tmpStr += myStr
            End If
        Next

        Return tmpStr
    End Function

#End Region

#Region "當冊當課所得結果寫回參考檔"

    '未習寫過新字 + 課文全新字 + 設定習寫字入參考檔
    ''' <summary>
    ''' 未習寫過新字 + 課文全新字 + 設定習寫字入參考檔
    ''' </summary>
    ''' <param name="outTxtFilePath">參考檔完整路徑</param>
    ''' <param name="bookNum">冊數索引</param>
    ''' <param name="lessonNum">課數索引</param>
    ''' <param name="misswWord">未習寫過新字字</param>
    ''' <param name="newLesson">課文全新字</param>
    ''' <param name="wWord">設定習字</param>
    ''' <param name="sug_Write">建議習寫字(依筆畫順序)</param>
    ''' <param name="sug_Read">建議認讀字</param>
    ''' <param name="noneSort_Write">建議習寫字(依課文順序)</param>
    ''' <param name="erase_Read">消化認讀字</param>
    ''' <param name="rWord">設定認讀字</param>
    ''' <param name="away_wStr">消失的設定習字</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function writein(ByVal outTxtFilePath As String, ByVal bookNum As Integer, ByVal lessonNum As Integer, ByVal misswWord As String, ByVal newLesson As String, ByVal wWord As String, ByVal sug_Write As String, ByVal sug_Read As String, ByVal noneSort_Write As String, ByVal erase_Read As String, ByVal rWord As String, ByVal away_wStr As String) As String
        writein = Nothing
        '冊課
        Dim ret As String = bookNum.ToString + "-" + lessonNum.ToString
        My.Computer.FileSystem.WriteAllText(outTxtFilePath, vbCrLf + ret + vbCrLf, True, System.Text.Encoding.Default)
        '課文全新字
        My.Computer.FileSystem.WriteAllText(outTxtFilePath, "課文新字:" + newLesson + vbCrLf, True, System.Text.Encoding.Default)
        '未習寫過字
        My.Computer.FileSystem.WriteAllText(outTxtFilePath, "未習寫過字:" + misswWord + vbCrLf, True, System.Text.Encoding.Default)
        '建議習寫字(依課文順序)
        My.Computer.FileSystem.WriteAllText(outTxtFilePath, "建議習寫字(依課文順序):" + noneSort_Write + vbCrLf, True, System.Text.Encoding.Default)
        '建議習寫字(依筆畫順序)
        My.Computer.FileSystem.WriteAllText(outTxtFilePath, "建議習寫字(依筆畫順序):" + sug_Write + vbCrLf, True, System.Text.Encoding.Default)
        '建議認讀字
        My.Computer.FileSystem.WriteAllText(outTxtFilePath, "建議認讀字:" + sug_Read + vbCrLf, True, System.Text.Encoding.Default)
        '設定習寫字
        My.Computer.FileSystem.WriteAllText(outTxtFilePath, "設定習寫字:" + wWord + vbCrLf, True, System.Text.Encoding.Default)
        '設定認讀字
        My.Computer.FileSystem.WriteAllText(outTxtFilePath, "設定認讀字:" + rWord + vbCrLf, True, System.Text.Encoding.Default)
        '消化認讀字
        My.Computer.FileSystem.WriteAllText(outTxtFilePath, "消化認讀字:" + erase_Read + vbCrLf, True, System.Text.Encoding.Default)
        '消失的設定習字
        My.Computer.FileSystem.WriteAllText(outTxtFilePath, "消失的設定習字:" + away_wStr + vbCrLf, True, System.Text.Encoding.Default)
    End Function

#End Region

#Region "來源字串中以參考字串為參考挑出或排除出現過的字"

    '從 sourceStr 中以 refStr 為參考挑出出現過的字
    Function pickupStr(ByVal sourceStr As String, ByVal refStr As String) As String
        pickupStr = Nothing

        If (sourceStr Is Nothing) Or (sourceStr = "") Then
            Return Nothing
        End If
        If (refStr Is Nothing) Or (refStr = "") Then
            Return Nothing
        End If

        Dim tmpStr As String = Nothing '字串收集暫存
        Dim i As Integer
        For i = 0 To sourceStr.Length - 1 '順序仍依照來源字串，決定是否累加進暫存變數字串，端視參考字串是否出現相同字
            Dim nowStr As String = sourceStr(i)
            Dim j As Integer
            For j = 0 To refStr.Length - 1 '在參考字串裡搜尋是否有符合字
                If nowStr = refStr(j) Then
                    tmpStr += nowStr
                    Exit For '跳出回圈 再從下一個來源字開始
                End If
            Next
        Next
        Return tmpStr '返回收集到的暫存字串
    End Function
    '從 sourceStr 中以 refStr 為參考挑未出現過的字
    Function revpickupStr(ByVal sourceStr As String, ByVal refStr As String) As String
        revpickupStr = Nothing
        If (sourceStr Is Nothing) Or (sourceStr = "") Then
            Return Nothing
        End If
        If (refStr Is Nothing) Or (refStr = "") Then
            Return sourceStr
        End If

        Dim tmpStr As String = Nothing '字串收集暫存
        Dim i As Integer
        For i = 0 To sourceStr.Length - 1 '順序仍依照來源字串，決定是否累加進暫存變數字串，端視參考字串是否出現相同字
            Dim nowStr As String = sourceStr(i)
            Dim ret As Boolean = False '是否有相同字出現在參考字串布林
            Dim j As Integer
            For j = 0 To refStr.Length - 1 '在參考字串裡搜尋是否有符合字
                If nowStr = refStr(j) Then
                    ret = True
                    Exit For '跳出回圈 再從下一個來源字開始
                End If
            Next
            If ret = False Then
                tmpStr += nowStr
            Else
                ret = False '復歸
            End If
        Next
        Return tmpStr '返回收集到的暫存字串
    End Function


#End Region


End Class



'字相關資料自定型別
Public Class wordData
    Implements IComparable ' 實作 IComparable 介面才會支援排序與二進位搜尋。

    ' 用來控制哪一個欄位將被用來比較。
    Private Shared m_CompareField As CompareField

    Public wordStr As String
    Public strokeNum As Integer
    Public id As Integer
    Public showCount As Integer

    Public Enum CompareField
        wordStr
        strokeNum
        id
        showCount
    End Enum

    ' 設定預設的比較欄位。
    Shared Sub New()
        m_CompareField = CompareField.strokeNum
    End Sub

    Public Shared Sub SetCompareKey(ByVal CompareKey As CompareField)
        ' 變更比較欄位。
        m_CompareField = CompareKey
    End Sub

    Public Sub New()
        ' 藉由委派給下一個最複雜的建構函式來設定預設值。
        Me.New("No Name", 0, 0, 0)
    End Sub

    Public Sub New(ByVal p_wordStr As String, ByVal p_strokeNum As Integer, ByVal p_id As Integer, ByVal p_showCount As String)
        Me.wordStr = p_wordStr
        Me.strokeNum = p_strokeNum
        Me.id = p_id
        Me.showCount = p_showCount
    End Sub

    Public Function CompareTo(ByVal obj As Object) As Integer Implements System.IComparable.CompareTo
        ' 首先會加以檢查以便確認我們只是要與另外一個字來比較。
        If TypeOf obj Is wordData Then
            ' 建立一個強型別參考。
            Dim c As wordData = CType(obj, wordData)
            ' 決定我們要使用哪個欄位來比較。
            Select Case wordData.m_CompareField
                Case CompareField.strokeNum '筆數
                    If c.strokeNum = Me.strokeNum Then
                        Return 0
                    ElseIf c.strokeNum < Me.strokeNum Then
                        Return 1
                    Else
                        Return -1
                    End If
                Case CompareField.wordStr '字
                    If c.wordStr = Me.wordStr Then
                        Return 0
                    ElseIf c.wordStr < Me.wordStr Then
                        Return 1
                    Else
                        Return -1
                    End If
                Case CompareField.id '序號
                    If c.id = Me.id Then
                        Return 0
                    ElseIf c.id < Me.id Then
                        Return 1
                    Else
                        Return -1
                    End If
                Case CompareField.showCount '字頻
                    If c.showCount = Me.showCount Then
                        Return 0
                    ElseIf c.showCount < Me.showCount Then
                        Return 1
                    Else
                        Return -1
                    End If
            End Select
        Else
            Throw New ArgumentException("字只能夠與其它的字來比較。所傳遞進來的物件是一個 " & obj.GetType.ToString())
        End If

    End Function

    Public Overrides Function ToString() As String
        ' 在正常情況下，ToString 會傳回完整的命名空間以及所有基本型別的名稱。
        ' 以本範例而言，它會傳回 comparetxt.wordData 。
        ' 我們將其覆寫，以便使它傳回代表 字 筆數 序號 字頻  的字串。
        Return String.Format("字={0}, 筆數={1} , 序號={2} , 字頻={3}", Me.wordStr, Me.strokeNum, Me.id, Me.showCount)
    End Function
End Class
