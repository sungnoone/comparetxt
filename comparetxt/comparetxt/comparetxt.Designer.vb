﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class comparetxt
    Inherits System.Windows.Forms.Form

    'Form 覆寫 Dispose 以清除元件清單。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    '為 Windows Form 設計工具的必要項
    Private components As System.ComponentModel.IContainer

    '注意: 以下為 Windows Form 設計工具所需的程序
    '可以使用 Windows Form 設計工具進行修改。
    '請不要使用程式碼編輯器進行修改。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(comparetxt))
        Me.lb1 = New System.Windows.Forms.ListBox()
        Me.bn1More = New System.Windows.Forms.Button()
        Me.bn1Less = New System.Windows.Forms.Button()
        Me.bn1Up = New System.Windows.Forms.Button()
        Me.bn1Down = New System.Windows.Forms.Button()
        Me.bn2Down = New System.Windows.Forms.Button()
        Me.bn2Up = New System.Windows.Forms.Button()
        Me.bn2Less = New System.Windows.Forms.Button()
        Me.bn2More = New System.Windows.Forms.Button()
        Me.lb2 = New System.Windows.Forms.ListBox()
        Me.bn3Down = New System.Windows.Forms.Button()
        Me.bn3Up = New System.Windows.Forms.Button()
        Me.bn3Less = New System.Windows.Forms.Button()
        Me.bn3More = New System.Windows.Forms.Button()
        Me.lb3 = New System.Windows.Forms.ListBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtOutput = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.bnRun = New System.Windows.Forms.Button()
        Me.gb1 = New System.Windows.Forms.GroupBox()
        Me.bn_openFolder = New System.Windows.Forms.Button()
        Me.msg = New System.Windows.Forms.TextBox()
        Me.bnTextCompare = New System.Windows.Forms.Button()
        Me.bnClearMsg = New System.Windows.Forms.Button()
        Me.gb2 = New System.Windows.Forms.GroupBox()
        Me.bn2Clear = New System.Windows.Forms.Button()
        Me.bn1Clear = New System.Windows.Forms.Button()
        Me.OpenInputFile = New System.Windows.Forms.OpenFileDialog()
        Me.procBar = New System.Windows.Forms.ProgressBar()
        Me.openFolder = New System.Windows.Forms.FolderBrowserDialog()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.bn_Array = New System.Windows.Forms.Button()
        Me.BindingSource1 = New System.Windows.Forms.BindingSource(Me.components)
        Me.dv = New System.Data.DataView()
        Me.dgv = New System.Windows.Forms.DataGridView()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.cbHandleNum = New System.Windows.Forms.ComboBox()
        Me.bn_getViewID = New System.Windows.Forms.Button()
        Me.bn3Clear = New System.Windows.Forms.Button()
        Me.gb1.SuspendLayout()
        Me.gb2.SuspendLayout()
        CType(Me.BindingSource1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dv, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgv, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'lb1
        '
        Me.lb1.FormattingEnabled = True
        Me.lb1.HorizontalScrollbar = True
        Me.lb1.ItemHeight = 12
        Me.lb1.Location = New System.Drawing.Point(37, 33)
        Me.lb1.Name = "lb1"
        Me.lb1.ScrollAlwaysVisible = True
        Me.lb1.Size = New System.Drawing.Size(220, 280)
        Me.lb1.TabIndex = 1
        '
        'bn1More
        '
        Me.bn1More.Location = New System.Drawing.Point(10, 92)
        Me.bn1More.Name = "bn1More"
        Me.bn1More.Size = New System.Drawing.Size(21, 24)
        Me.bn1More.TabIndex = 2
        Me.bn1More.Text = "＋"
        Me.ToolTip1.SetToolTip(Me.bn1More, "加入課文檔清單")
        Me.bn1More.UseVisualStyleBackColor = True
        '
        'bn1Less
        '
        Me.bn1Less.Location = New System.Drawing.Point(10, 133)
        Me.bn1Less.Name = "bn1Less"
        Me.bn1Less.Size = New System.Drawing.Size(21, 24)
        Me.bn1Less.TabIndex = 3
        Me.bn1Less.Text = "－"
        Me.ToolTip1.SetToolTip(Me.bn1Less, "移除單筆資料")
        Me.bn1Less.UseVisualStyleBackColor = True
        '
        'bn1Up
        '
        Me.bn1Up.Location = New System.Drawing.Point(10, 49)
        Me.bn1Up.Name = "bn1Up"
        Me.bn1Up.Size = New System.Drawing.Size(21, 24)
        Me.bn1Up.TabIndex = 4
        Me.bn1Up.Text = "↑"
        Me.ToolTip1.SetToolTip(Me.bn1Up, "上移單筆資料")
        Me.bn1Up.UseVisualStyleBackColor = True
        '
        'bn1Down
        '
        Me.bn1Down.Location = New System.Drawing.Point(10, 172)
        Me.bn1Down.Name = "bn1Down"
        Me.bn1Down.Size = New System.Drawing.Size(21, 24)
        Me.bn1Down.TabIndex = 5
        Me.bn1Down.Text = "↓"
        Me.ToolTip1.SetToolTip(Me.bn1Down, "下移單筆記錄")
        Me.bn1Down.UseVisualStyleBackColor = True
        '
        'bn2Down
        '
        Me.bn2Down.Location = New System.Drawing.Point(264, 172)
        Me.bn2Down.Name = "bn2Down"
        Me.bn2Down.Size = New System.Drawing.Size(21, 24)
        Me.bn2Down.TabIndex = 10
        Me.bn2Down.Text = "↓"
        Me.ToolTip1.SetToolTip(Me.bn2Down, "下移單筆記錄")
        Me.bn2Down.UseVisualStyleBackColor = True
        '
        'bn2Up
        '
        Me.bn2Up.Location = New System.Drawing.Point(264, 49)
        Me.bn2Up.Name = "bn2Up"
        Me.bn2Up.Size = New System.Drawing.Size(21, 24)
        Me.bn2Up.TabIndex = 9
        Me.bn2Up.Text = "↑"
        Me.ToolTip1.SetToolTip(Me.bn2Up, "上移單筆資料")
        Me.bn2Up.UseVisualStyleBackColor = True
        '
        'bn2Less
        '
        Me.bn2Less.Location = New System.Drawing.Point(264, 133)
        Me.bn2Less.Name = "bn2Less"
        Me.bn2Less.Size = New System.Drawing.Size(21, 24)
        Me.bn2Less.TabIndex = 8
        Me.bn2Less.Text = "－"
        Me.ToolTip1.SetToolTip(Me.bn2Less, "移除單筆資料")
        Me.bn2Less.UseVisualStyleBackColor = True
        '
        'bn2More
        '
        Me.bn2More.Location = New System.Drawing.Point(264, 92)
        Me.bn2More.Name = "bn2More"
        Me.bn2More.Size = New System.Drawing.Size(21, 24)
        Me.bn2More.TabIndex = 7
        Me.bn2More.Text = "＋"
        Me.ToolTip1.SetToolTip(Me.bn2More, "加入習字檔清單")
        Me.bn2More.UseVisualStyleBackColor = True
        '
        'lb2
        '
        Me.lb2.FormattingEnabled = True
        Me.lb2.HorizontalScrollbar = True
        Me.lb2.ItemHeight = 12
        Me.lb2.Location = New System.Drawing.Point(291, 33)
        Me.lb2.Name = "lb2"
        Me.lb2.ScrollAlwaysVisible = True
        Me.lb2.Size = New System.Drawing.Size(220, 280)
        Me.lb2.TabIndex = 6
        '
        'bn3Down
        '
        Me.bn3Down.Location = New System.Drawing.Point(521, 172)
        Me.bn3Down.Name = "bn3Down"
        Me.bn3Down.Size = New System.Drawing.Size(21, 24)
        Me.bn3Down.TabIndex = 15
        Me.bn3Down.Text = "↓"
        Me.bn3Down.UseVisualStyleBackColor = True
        '
        'bn3Up
        '
        Me.bn3Up.Location = New System.Drawing.Point(521, 49)
        Me.bn3Up.Name = "bn3Up"
        Me.bn3Up.Size = New System.Drawing.Size(21, 24)
        Me.bn3Up.TabIndex = 14
        Me.bn3Up.Text = "↑"
        Me.bn3Up.UseVisualStyleBackColor = True
        '
        'bn3Less
        '
        Me.bn3Less.Location = New System.Drawing.Point(521, 133)
        Me.bn3Less.Name = "bn3Less"
        Me.bn3Less.Size = New System.Drawing.Size(21, 24)
        Me.bn3Less.TabIndex = 13
        Me.bn3Less.Text = "－"
        Me.bn3Less.UseVisualStyleBackColor = True
        '
        'bn3More
        '
        Me.bn3More.Location = New System.Drawing.Point(521, 92)
        Me.bn3More.Name = "bn3More"
        Me.bn3More.Size = New System.Drawing.Size(21, 24)
        Me.bn3More.TabIndex = 12
        Me.bn3More.Text = "＋"
        Me.bn3More.UseVisualStyleBackColor = True
        '
        'lb3
        '
        Me.lb3.FormattingEnabled = True
        Me.lb3.HorizontalScrollbar = True
        Me.lb3.ItemHeight = 12
        Me.lb3.Location = New System.Drawing.Point(548, 33)
        Me.lb3.Name = "lb3"
        Me.lb3.ScrollAlwaysVisible = True
        Me.lb3.Size = New System.Drawing.Size(220, 280)
        Me.lb3.TabIndex = 11
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(70, 18)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(41, 12)
        Me.Label1.TabIndex = 16
        Me.Label1.Text = "課文檔"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(319, 18)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(53, 12)
        Me.Label2.TabIndex = 17
        Me.Label2.Text = "習寫字檔"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(568, 18)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(53, 12)
        Me.Label3.TabIndex = 18
        Me.Label3.Text = "認讀字檔"
        '
        'txtOutput
        '
        Me.txtOutput.BackColor = System.Drawing.Color.White
        Me.txtOutput.Location = New System.Drawing.Point(8, 32)
        Me.txtOutput.Name = "txtOutput"
        Me.txtOutput.ReadOnly = True
        Me.txtOutput.Size = New System.Drawing.Size(123, 22)
        Me.txtOutput.TabIndex = 19
        Me.ToolTip1.SetToolTip(Me.txtOutput, "選擇輸出檔案要存放位置")
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(6, 17)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(77, 12)
        Me.Label5.TabIndex = 21
        Me.Label5.Text = "輸出檔案路徑"
        '
        'bnRun
        '
        Me.bnRun.Font = New System.Drawing.Font("新細明體", 12.0!)
        Me.bnRun.Location = New System.Drawing.Point(6, 302)
        Me.bnRun.Name = "bnRun"
        Me.bnRun.Size = New System.Drawing.Size(58, 39)
        Me.bnRun.TabIndex = 22
        Me.bnRun.Text = "執行"
        Me.bnRun.UseVisualStyleBackColor = True
        '
        'gb1
        '
        Me.gb1.Controls.Add(Me.bn_openFolder)
        Me.gb1.Controls.Add(Me.msg)
        Me.gb1.Controls.Add(Me.txtOutput)
        Me.gb1.Controls.Add(Me.Label5)
        Me.gb1.Location = New System.Drawing.Point(6, 12)
        Me.gb1.Name = "gb1"
        Me.gb1.Size = New System.Drawing.Size(159, 262)
        Me.gb1.TabIndex = 23
        Me.gb1.TabStop = False
        Me.gb1.Text = "參數"
        '
        'bn_openFolder
        '
        Me.bn_openFolder.Location = New System.Drawing.Point(131, 33)
        Me.bn_openFolder.Name = "bn_openFolder"
        Me.bn_openFolder.Size = New System.Drawing.Size(22, 21)
        Me.bn_openFolder.TabIndex = 19
        Me.bn_openFolder.Text = "..."
        Me.ToolTip1.SetToolTip(Me.bn_openFolder, "選擇存放位置")
        Me.bn_openFolder.UseVisualStyleBackColor = True
        '
        'msg
        '
        Me.msg.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.msg.Location = New System.Drawing.Point(6, 60)
        Me.msg.Multiline = True
        Me.msg.Name = "msg"
        Me.msg.ReadOnly = True
        Me.msg.ScrollBars = System.Windows.Forms.ScrollBars.Both
        Me.msg.Size = New System.Drawing.Size(147, 193)
        Me.msg.TabIndex = 22
        '
        'bnTextCompare
        '
        Me.bnTextCompare.Location = New System.Drawing.Point(6, 425)
        Me.bnTextCompare.Name = "bnTextCompare"
        Me.bnTextCompare.Size = New System.Drawing.Size(21, 24)
        Me.bnTextCompare.TabIndex = 23
        Me.bnTextCompare.Text = "T"
        Me.bnTextCompare.UseVisualStyleBackColor = True
        '
        'bnClearMsg
        '
        Me.bnClearMsg.Location = New System.Drawing.Point(70, 302)
        Me.bnClearMsg.Name = "bnClearMsg"
        Me.bnClearMsg.Size = New System.Drawing.Size(95, 23)
        Me.bnClearMsg.TabIndex = 19
        Me.bnClearMsg.Text = "清除訊息"
        Me.bnClearMsg.UseVisualStyleBackColor = True
        '
        'gb2
        '
        Me.gb2.Controls.Add(Me.bn3Clear)
        Me.gb2.Controls.Add(Me.bn2Clear)
        Me.gb2.Controls.Add(Me.bn1Clear)
        Me.gb2.Controls.Add(Me.lb1)
        Me.gb2.Controls.Add(Me.bn1More)
        Me.gb2.Controls.Add(Me.bn1Less)
        Me.gb2.Controls.Add(Me.Label3)
        Me.gb2.Controls.Add(Me.bn1Up)
        Me.gb2.Controls.Add(Me.Label2)
        Me.gb2.Controls.Add(Me.bn1Down)
        Me.gb2.Controls.Add(Me.Label1)
        Me.gb2.Controls.Add(Me.lb2)
        Me.gb2.Controls.Add(Me.bn3Down)
        Me.gb2.Controls.Add(Me.bn2More)
        Me.gb2.Controls.Add(Me.bn3Up)
        Me.gb2.Controls.Add(Me.bn2Less)
        Me.gb2.Controls.Add(Me.bn3Less)
        Me.gb2.Controls.Add(Me.bn2Up)
        Me.gb2.Controls.Add(Me.bn3More)
        Me.gb2.Controls.Add(Me.bn2Down)
        Me.gb2.Controls.Add(Me.lb3)
        Me.gb2.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.gb2.Location = New System.Drawing.Point(171, 12)
        Me.gb2.Name = "gb2"
        Me.gb2.Size = New System.Drawing.Size(775, 328)
        Me.gb2.TabIndex = 24
        Me.gb2.TabStop = False
        Me.gb2.Text = "檔案清單"
        '
        'bn2Clear
        '
        Me.bn2Clear.Location = New System.Drawing.Point(264, 214)
        Me.bn2Clear.Name = "bn2Clear"
        Me.bn2Clear.Size = New System.Drawing.Size(21, 24)
        Me.bn2Clear.TabIndex = 20
        Me.bn2Clear.Text = "C"
        Me.ToolTip1.SetToolTip(Me.bn2Clear, "清除習字檔清單")
        Me.bn2Clear.UseVisualStyleBackColor = True
        '
        'bn1Clear
        '
        Me.bn1Clear.Location = New System.Drawing.Point(10, 214)
        Me.bn1Clear.Name = "bn1Clear"
        Me.bn1Clear.Size = New System.Drawing.Size(21, 24)
        Me.bn1Clear.TabIndex = 19
        Me.bn1Clear.Text = "C"
        Me.ToolTip1.SetToolTip(Me.bn1Clear, "清除課文檔清單")
        Me.bn1Clear.UseVisualStyleBackColor = True
        '
        'OpenInputFile
        '
        Me.OpenInputFile.Multiselect = True
        '
        'procBar
        '
        Me.procBar.Location = New System.Drawing.Point(6, 280)
        Me.procBar.Name = "procBar"
        Me.procBar.Size = New System.Drawing.Size(159, 16)
        Me.procBar.TabIndex = 19
        '
        'bn_Array
        '
        Me.bn_Array.Location = New System.Drawing.Point(6, 455)
        Me.bn_Array.Name = "bn_Array"
        Me.bn_Array.Size = New System.Drawing.Size(46, 24)
        Me.bn_Array.TabIndex = 25
        Me.bn_Array.Text = "Array"
        Me.bn_Array.UseVisualStyleBackColor = True
        '
        'dgv
        '
        Me.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgv.Location = New System.Drawing.Point(131, 373)
        Me.dgv.Name = "dgv"
        Me.dgv.RowTemplate.Height = 24
        Me.dgv.Size = New System.Drawing.Size(536, 137)
        Me.dgv.TabIndex = 26
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(4, 384)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(65, 12)
        Me.Label4.TabIndex = 28
        Me.Label4.Text = "處裡總冊數"
        '
        'cbHandleNum
        '
        Me.cbHandleNum.FormattingEnabled = True
        Me.cbHandleNum.Location = New System.Drawing.Point(4, 399)
        Me.cbHandleNum.Name = "cbHandleNum"
        Me.cbHandleNum.Size = New System.Drawing.Size(121, 20)
        Me.cbHandleNum.TabIndex = 27
        '
        'bn_getViewID
        '
        Me.bn_getViewID.Location = New System.Drawing.Point(70, 331)
        Me.bn_getViewID.Name = "bn_getViewID"
        Me.bn_getViewID.Size = New System.Drawing.Size(95, 23)
        Me.bn_getViewID.TabIndex = 21
        Me.bn_getViewID.Text = "清單ID"
        Me.bn_getViewID.UseVisualStyleBackColor = True
        Me.bn_getViewID.Visible = False
        '
        'bn3Clear
        '
        Me.bn3Clear.Location = New System.Drawing.Point(521, 214)
        Me.bn3Clear.Name = "bn3Clear"
        Me.bn3Clear.Size = New System.Drawing.Size(21, 24)
        Me.bn3Clear.TabIndex = 21
        Me.bn3Clear.Text = "C"
        Me.ToolTip1.SetToolTip(Me.bn3Clear, "清除習字檔清單")
        Me.bn3Clear.UseVisualStyleBackColor = True
        '
        'comparetxt
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 12.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(949, 358)
        Me.Controls.Add(Me.bn_getViewID)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.bnClearMsg)
        Me.Controls.Add(Me.bnTextCompare)
        Me.Controls.Add(Me.cbHandleNum)
        Me.Controls.Add(Me.dgv)
        Me.Controls.Add(Me.bn_Array)
        Me.Controls.Add(Me.procBar)
        Me.Controls.Add(Me.gb2)
        Me.Controls.Add(Me.gb1)
        Me.Controls.Add(Me.bnRun)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.Name = "comparetxt"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "國小跑生字(v2.02)"
        Me.gb1.ResumeLayout(False)
        Me.gb1.PerformLayout()
        Me.gb2.ResumeLayout(False)
        Me.gb2.PerformLayout()
        CType(Me.BindingSource1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dv, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgv, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lb1 As System.Windows.Forms.ListBox
    Friend WithEvents bn1More As System.Windows.Forms.Button
    Friend WithEvents bn1Less As System.Windows.Forms.Button
    Friend WithEvents bn1Up As System.Windows.Forms.Button
    Friend WithEvents bn1Down As System.Windows.Forms.Button
    Friend WithEvents bn2Down As System.Windows.Forms.Button
    Friend WithEvents bn2Up As System.Windows.Forms.Button
    Friend WithEvents bn2Less As System.Windows.Forms.Button
    Friend WithEvents bn2More As System.Windows.Forms.Button
    Friend WithEvents lb2 As System.Windows.Forms.ListBox
    Friend WithEvents bn3Down As System.Windows.Forms.Button
    Friend WithEvents bn3Up As System.Windows.Forms.Button
    Friend WithEvents bn3Less As System.Windows.Forms.Button
    Friend WithEvents bn3More As System.Windows.Forms.Button
    Friend WithEvents lb3 As System.Windows.Forms.ListBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtOutput As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents bnRun As System.Windows.Forms.Button
    Friend WithEvents gb1 As System.Windows.Forms.GroupBox
    Friend WithEvents gb2 As System.Windows.Forms.GroupBox
    Friend WithEvents OpenInputFile As System.Windows.Forms.OpenFileDialog
    Friend WithEvents msg As System.Windows.Forms.TextBox
    Friend WithEvents bnClearMsg As System.Windows.Forms.Button
    Friend WithEvents bnTextCompare As System.Windows.Forms.Button
    Friend WithEvents procBar As System.Windows.Forms.ProgressBar
    Friend WithEvents openFolder As System.Windows.Forms.FolderBrowserDialog
    Friend WithEvents bn_openFolder As System.Windows.Forms.Button
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents bn_Array As System.Windows.Forms.Button
    Friend WithEvents BindingSource1 As System.Windows.Forms.BindingSource
    Friend WithEvents dv As System.Data.DataView
    Friend WithEvents dgv As System.Windows.Forms.DataGridView
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents cbHandleNum As System.Windows.Forms.ComboBox
    Friend WithEvents bn1Clear As System.Windows.Forms.Button
    Friend WithEvents bn2Clear As System.Windows.Forms.Button
    Friend WithEvents bn_getViewID As System.Windows.Forms.Button
    Friend WithEvents bn3Clear As System.Windows.Forms.Button

End Class
